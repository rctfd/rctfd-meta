# Summary

[Landing Page](./README.md)

- [Vision](./VISION.md)
- [Process](./process/README.md)
  - [Documentation](./process/DOCUMENTATION.md)
  - [New Repositories](./process/NEW_REPOS.md)
