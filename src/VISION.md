# Requirements
- [ ] extreme plugin support, maximising interoperability
  - [ ] cross-plugin support
- [ ] i18n support
- [ ] documentation
  - [ ] developer documentation
  - [ ] ctf organiser documentation
- [ ] examples

## Some Goals for Supported Plugins
- [ ] internationalization (i18n)
- [ ] thorough documentation
- [ ] user verification via email, 2FA, captcha
- [ ] web frontend
  - [ ] accessibility (a11y)
- [ ] [json-scoreboard-feed](https://ctftime.org/json-scoreboard-feed) support
- [ ] user registration
- [ ] user verification

## Plugin Ideas
- [ ] bulk challenge import from non-web sources
- [ ] network based adapter for views (model as a server, view as client)
- [ ] telnet based ctf-view
- [ ] ssh based ctf-view
- [ ] web based ctf-view
- [ ] support for dynamic and static scoring systems
- [ ] storage options [mysql, psql, lucid, etc.]
