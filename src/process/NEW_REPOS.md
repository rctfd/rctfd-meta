# New Repositories

1. [Use the template for new repositories][template]
2. Update the Cargo.toml with authors and project name
3. Update the README with relevant information
4. Remove the example code and replace it with your code, retaining `forbid` and
  `deny` declarations
5. Update "Merge requests" in Settings > General to enforce "Pipelines must
  succeed"
6. Update "Merge request approvers" in Settings > General to enforce:
  - 1 code approver
  - "Prevent approval of merge requests by merge request committers"
7. Enable Discord Notifications in Integrations with a webhook to
  `#all-notifications` with:
  - ALL triggers enabled
  - "Notify on broken pipelines" enabled
  - "Branches to be notified" => "All branches"
8. Provide a basic description in doc/README.md, or delete the directory if you
  only need developer documentation (if the latter, remove the task in the CI
  configuration)
9. Commit and push your initial commit and ensure the code builds
10. Update "Protected Branches" in Settings > Repository to have:
  - Branch => master
  - Allowed to merge => Maintainers
  - Allowed to push => No one
  - Code owner approval => Disabled

[template]: https://gitlab.com/rctfd/internal/rctfd-template
