# Documentation

Some rules for documenting codebases and features.

## For Developers

This should be fairly self-handled -- the new repo template enforces
documentation on public methods and generates the docs.

## For Users

Unsure how to handle this. We'll come back to this. // TODO
